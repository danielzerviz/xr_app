
    import { formatDate } from './helper.js';
    let client = ZAFClient.init();
    
    client.invoke('resize', { width: '100%', height: '200px' });

    // obtener el id del solicitante de un ticket
    client.get('ticket.requester.id').then(
      function(data) {
        let user_id = data['ticket.requester.id'];
        console.log(`user id ${user_id}`);
        requestUserInfo(client, user_id);
      }
    );

    // obtener el usuario del ticket actual por su id
    const requestUserInfo = (client, id) => {
      var settings = {
        url: '/api/v2/users/' + id + '.json',
        type:'GET',
        dataType: 'json',
      };
    
      client.request(settings).then(
        function(data) {
          showInfo(data);
        },
        function(response) {
          console.log(response)
          showError(response);
        }
      );
    }

    const showInfo = (data) => {
        let requester_data = {
          'name': data.user.name,
          'tags': data.user.tags,
          'created_at': formatDate(data.user.created_at),
          'last_login_at': formatDate(data.user.last_login_at)
        };
        
        // var source = $("#requester-template").html();
        let source = document.querySelector("#requester-template").innerHTML;
        let template = Handlebars.compile(source);
        let html = template(requester_data);
        document.querySelector("#content").innerHTML = html;
        // $("#content").html(html);
      }
    
    const  showError = (response) => {
        let error_data = {
          'status': response.status,
          'statusText': response.responseJSON.error.message
        };
        // let source = $("#error-template").html();
        let source = document.querySelector('#error-template').innerHTML;
        let template = Handlebars.compile(source);
        let html = template(error_data);
        // $("#content").html(html);
        document.querySelector('#content').innerHTML = html;
    }